function toggleNav () {
	document.getElementById("side-bar").classList.add("active");
}
function closeNav () {
	document.getElementById("side-bar").classList.remove("active");
}
$(document).scroll(function() {
	if (window.pageYOffset>=400) {
		document.getElementById("navigate").classList.add("active");
	}else if (window.pageYOffset<10) {
		document.getElementById("navigate").classList.remove("active");
	}
});
$('.owl-carousel').owlCarousel({
    loop:true,
    nav:true,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});